// 公共类缓存
import store from '@/store';
import storage from '@/common/storage.js';
import device from '@/common/device.js';
import utils from '@/common/utils.js';
import wechatLogin from '@/common/wechat/login.js';
import wechatShare from '@/common/wechat/share.js';
import sign from '@/common/sign.js';
const install = (Vue, vm) => {

	vm.$u.common = {
		...storage,
		...device,
		...utils,
		...wechatLogin.install(Vue, vm),
		...wechatShare.install(Vue, vm),
		...sign,

		/**
		 * 网页meta信息
		 * @param {Object} param -title、icon、description信息
		 */
		setWebInfo(param) { },
		// 设置环境缓存
		setEnvStorage(key, value, bool) {
			if (typeof value != 'string') {
				value = JSON.stringify(value)
			}
			if (bool) uni.setStorage(`${process.env.NODE_ENV}_${key}`, value)
			else uni.setStorageSync(`${process.env.NODE_ENV}_${key}`, value)
		},

		// 获取环境缓存
		getEnvStorage(key, bool) {
			if (bool) return uni.getStorage(`${process.env.NODE_ENV}_${key}`)
			else return uni.getStorageSync(`${process.env.NODE_ENV}_${key}`)
		},

		removeEnvStorage(key, bool) {
			if (bool) uni.removeStorage(`${process.env.NODE_ENV}_${key}`)
			else uni.removeStorageSync(`${process.env.NODE_ENV}_${key}`)
		},

		clearLoginInfo() {
			this.removeEnvStorage('user')
			this.removeEnvStorage('token')
		},

		back() {
			var pages = getCurrentPages();
			if (pages.length > 1) {
				try {
					uni.navigateBack();
				} catch (err) {
					uni.reLaunch({
						url: `/pages/product/index`
					});
				}
			} else {
				uni.reLaunch({
					url: `/pages/product/index`
				});
			}
		},

		showFailToast(title, m = 1500) {
			// 提示错误
			uni.showToast({
				title: title,
				icon: 'none',
				duration: m,
				mask: true
			})
		},

		formatValueUtil(value, count, unit = 1, positive) {
			if (positive) {
				value = Math.abs(value);
			}
			if (value === 0) {
				return 0
			}
			if (!value) {
				return ''
			}
			if (unit == 1000) {
				// 千为单位
				value = value / unit
			} else if (unit == 10000) {
				// 万为单位
				value = value / unit
			}
			return value.toFixed(count) // 此处保留小数，保留几位小数，这里写几
		},

		getDistance(point1, point2) {
			let PI = 3.14159265358979323; //圆周率
			let R = 6371229; //地球半径

			var {
				lng: lon1,
				lat: lat1
			} = point1;
			var {
				lng: lon2,
				lat: lat2
			} = point2;

			let x, y, distance;
			let lonres = lon1 > lon2 ? lon1 - lon2 : lon2 - lon1;
			let latres = lat1 > lat2 ? lat1 - lat2 : lat2 - lat1;
			x = (lonres) * PI * R * Math.cos(((lat1 + lat2) / 2) * PI / 180) / 180;
			y = (lat2 - lat1) * PI * R / 180;
			distance = Math.hypot(x, y);
			return distance
		},

		callPhone(phoneNumber) {
			let app = getApp()
			app.globalData.isSystemMask = true
			uni.makePhoneCall({
				phoneNumber
			});
		},

		// 获取今日销售接口
		getShopTodaySales(shopId) {
			return new Promise((resolve, reject) => {
				let params = {
					shopId
				}
				vm.$u.api.getShopTodaySales(params).then(res => {
					if (res.code == 0 && res.result) {
						resolve(res.result.sales || {})
					} else {
						reject(res)
					}
				}).catch(err => {
					reject(err)
				})
			})
		},

		strcharacterDiscode(str) {
			// 加入常用解析
			str = str.replace(/&nbsp;/g, ' ');
			str = str.replace(/&quot;/g, "'");
			str = str.replace(/&amp;/g, '&');
			// str = str.replace(/&lt;/g, '‹');
			// str = str.replace(/&gt;/g, '›');

			str = str.replace(/&lt;/g, '<');
			str = str.replace(/&gt;/g, '>');
			str = str.replace(/&#8226;/g, '•');
			let newContent = str.replace(/<img[^>]*>/gi, function (match, capture) {
				match = match.replace(/style="[^"]+"/gi, '').replace(/style='[^']+'/gi, '');
				match = match.replace(/width="[^"]+"/gi, '').replace(/width='[^']+'/gi, '');
				match = match.replace(/height="[^"]+"/gi, '').replace(/height='[^']+'/gi, '');
				return match;
			});
			newContent = newContent.replace(/style="[^"]+"/gi, function (match, capture) {
				match = match.replace(/font-size:[^;]+;/gi, 'font-size: 50rpx;').replace(
					/font-size:[^;]+;/gi,
					'font-size: 50rpx;');
				return match;
			});
			newContent = newContent.replace(/<br[^>]*\/>/gi, '');
			newContent = newContent.replace(/&sup2;/gi, '²');
			newContent = newContent.replace(/\<img/gi,
				'<img style="max-width:100%;height:auto;display:block;margin-top:0;margin-bottom:0;"');
			return newContent;
		},

		// 判断数组是null或者空
		isArray(array) {
			if (array != null) {
				if (array.length > 0) {
					return true
				} else {
					return false
				}
			} else {
				return false
			}
		},

		behaviorReport(params) {
			params.openId = getApp().globalData.openId
			params.contentType = 'form'
			vm.$u.api.behaviorReport(params)
		},

		getImgTempPath(img) {
			return new Promise((resolve, reject) => {
				uni.downloadFile({
					url: img,
					success: res => {
						resolve(res)
					},
					fail: err => {
						reject(err)
					}
				})
			})
		},

		// 绘制销售名片
		creatSalePoster(sale, root) {
			let {
				avatar,
				name,
				tagList,
				serveNum,
				shop,
				mobile
			} = sale
			let {
				name: shopName,
				address,
				smacode
			} = shop
			avatar = avatar.replace('!reduce', '')
			smacode = smacode.replace('!reduce', '')
			let topViews = [{
				type: "text",
				text: '销售名片',
				css: {
					display: 'block',
					fontSize: '24rpx',
					textAlign: 'center',
					color: '#CCCCCC'
				},
			},
			{
				type: 'image',
				src: avatar,
				css: {
					display: 'block',
					margin: '46rpx auto 0',
					objectFit: 'cover',
					width: "160rpx",
					height: "160rpx",
					borderRadius: '80rpx'
				}
			},
			{
				type: "text",
				text: name,
				css: {
					display: 'block',
					marginTop: '40rpx',
					fontSize: '36rpx',
					fontWeight: 'bold',
					textAlign: 'center',
					color: '#000000'
				}
			}
			]

			if (tagList.length > 0) {
				topViews.push({
					type: 'text',
					text: tagList[0].tag,
					css: {
						margin: '8rpx auto 0',
						width: tagList[0].tag.length * 20 + 'rpx',
						height: '28rpx',
						lineHeight: '28rpx',
						border: '1rpx solid #D3382F',
						borderRadius: '4rpx',
						padding: '0 10rpx',
						fontSize: '20rpx',
						color: '#D3382F'
					}
				})
			}
			topViews.push({
				type: 'view',
				css: {
					display: 'flex',
					justifyContent: 'center',
					marginTop: '16rpx'
				},
				views: [{
					display: 'block',
					type: 'text',
					text: '服务过',
					css: {
						width: '100rpx',
						fontSize: '28rpx',
						textAlign: 'center',
						color: '#999999'
					}
				}, {
					type: 'text',
					text: serveNum || '0',
					css: {
						width: Math.ceil(serveNum / 10) + 1 * 25 + 'rpx',
						fontSize: '28rpx',
						fontWeight: 'bold',
						color: '#2E6BE5',
						textAlign: 'center'
					}
				}, {
					type: 'text',
					text: '位客户',
					css: {
						width: '100rpx',
						fontSize: '28rpx',
						textAlign: 'center',
						color: '#999999'
					}
				}]
			})
			let bottomView = [{
				type: 'view',
				css: {
					height: '66rpx',
					width: '100%'
				}
			}, {
				type: 'view',
				css: {
					display: 'flex',
					alignItems: 'center',
					marginBottom: '4rpx',
					padding: '0 24rpx',
					width: '502rpx',
					height: '64rpx',
					borderRadius: '4rpx',
					background: '#F7F8FC'
				},
				views: [{
					type: 'image',
					src: `${require('@/static/images/icon_store.png')}`,
					css: {
						width: '32rpx',
						height: '32rpx'
					}
				}, {
					type: 'text',
					text: '经销商：' + shopName,
					css: {
						marginLeft: '18rpx',
						fontSize: '24rpx',
						color: '#666666'
					}
				}]
			}, {
				type: 'view',
				css: {
					display: 'flex',
					alignItems: 'center',
					marginBottom: '4rpx',
					padding: '0 24rpx',
					width: '502rpx',
					height: '64rpx',
					borderRadius: '4rpx',
					background: '#F7F8FC'
				},
				views: [{
					type: 'image',
					src: `${require('@/static/images/icon_location.png')}`,
					css: {
						width: '32rpx',
						height: '32rpx'
					}
				}, {
					type: 'text',
					text: '所在位置：' + address,
					css: {
						marginLeft: '18rpx',
						fontSize: '24rpx',
						width: '452rpx',
						lineClamp: '1',
						color: '#666666'
					}
				}]
			}, {
				type: 'view',
				css: {
					display: 'flex',
					alignItems: 'center',
					marginBottom: '4rpx',
					padding: '0 24rpx',
					width: '502rpx',
					height: '64rpx',
					borderRadius: '4rpx',
					background: '#F7F8FC'
				},
				views: [{
					type: 'image',
					src: `${require('@/static/images/icon_call.png')}`,
					css: {
						width: '32rpx',
						height: '32rpx'
					}
				}, {
					type: 'text',
					text: '手机号码：' + mobile,
					css: {
						marginLeft: '18rpx',
						fontSize: '24rpx',
						color: '#666666'
					}
				}]
			}, {
				type: 'view',
				css: {
					display: 'flex',
					alignItems: 'center',
					marginBottom: '4rpx',
					padding: '0 24rpx',
					width: '502rpx',
					height: '176rpx',
					borderRadius: '4rpx',
					background: '#F7F8FC'
				},
				views: [{
					type: 'image',
					src: smacode,
					css: {
						height: '128rpx',
						width: '128rpx'
					}
				}, {
					type: 'view',
					css: {
						marginLeft: '24rpx'
					},
					views: [{
						type: 'text',
						text: '扫一扫，了解更多信息',
						css: {
							marginBottom: '16rpx',
							fontSize: '28rpx',
							fontWeight: 'bold',
							color: '#000000'
						}
					}, {
						type: 'text',
						text: '打开抖音扫码即可查看',
						css: {
							fontSize: '24rpx',
							color: '#666666'
						}
					}]
				}]
			}]

			let views = [{
				type: 'view',
				css: {
					padding: '40rpx'
				},
				views: topViews.concat(bottomView)
			}];
			return views
		},

		/**
		 * 查询经纬度授权状态
		 * @param {function} callback 
		 */
		getUserLocation: function (callback) {
			let vm = this
			uni.getSetting({
				success: (res) => {
					// res.authSetting['scope.userLocation'] == undefined  表示 初始化进入该页面
					// res.authSetting['scope.userLocation'] == false  表示 非初始化进入该页面,且未授权
					// res.authSetting['scope.userLocation'] == true  表示 地理位置授权
					// 拒绝授权后再次进入重新授权
					if (res.authSetting['scope.userLocation'] != undefined && res.authSetting[
						'scope.userLocation'] != true) {
						// console.log('authSetting:status:拒绝授权后再次进入重新授权', res.authSetting['scope.userLocation'])
						uni.showModal({
							title: '',
							content: '需要获取你的地理位置，请确认授权',
							success: function (res) {
								if (res.confirm) {
									uni.openSetting({
										success: function (dataAu) {
											// console.log('dataAu:success', dataAu)
											if (dataAu.authSetting[
												"scope.userLocation"
											] == true) {
												//再次授权，调用uni.getLocation的API
												vm.getLocation(dataAu)
													.then(res => {
														callback &&
															callback(
																res)
														vm.uploadPosition(res)
													})
											} else {
												vm.showFailToast('授权失败')
											}
										}
									})
								} else {
									vm.showFailToast('拒绝授权')
									callback && callback(null)
								}
							}
						})
					}
					// 初始化进入，未授权
					else if (res.authSetting['scope.userLocation'] == undefined) {
						// console.log('authSetting:status:初始化进入，未授权', res.authSetting['scope.userLocation'])
						//调用uni.getLocation的API
						vm.getLocation(res, callback).then(res => {
							vm.uploadPosition(res)
							callback && callback(res)
						})
					}
					// 已授权
					else if (res.authSetting['scope.userLocation']) {
						// console.log('authSetting:status:已授权', res.authSetting['scope.userLocation'])
						//调用uni.getLocation的API
						if (!getApp().globalData.location) {
							// console.log('已授权-1')
							vm.getLocation(res).then(res => {
								// console.log('已授权-1',res)
								vm.uploadPosition(res)
								callback && callback(res)
							})
							// this.onLocationChange()
						} else {
							let location = getApp().globalData.location
							// console.log('已授权-2',location)
							callback && callback(location)
						}
					}
				}
			})
		},

		/**
		 * 微信获得经纬度
		 * @param {object} userLocation 
		 */
		getLocation: function (userLocation, callback) {
			let vm = this
			return new Promise((resolve, reject) => {
				uni.getLocation({
					type: "gcj02",
					success: function (res) {
						// console.log('getLocation:success', res)
						// var latitude = res.latitude
						// var longitude = res.longitude

						// 保存全局地理位置
						getApp().globalData.location = res
						vm.uploadPosition(res)
						resolve(res)
					},
					fail: function (res) {
						// console.log('getLocation:fail', res)
						if (res.errMsg === 'getLocation:fail:auth denied') {
							vm.showFailToast('拒绝授权')
							return
						}
						if (!userLocation || !userLocation.authSetting[
							'scope.userLocation']) {
							vm.getUserLocation(callback || null)
						} else if (userLocation.authSetting['scope.userLocation']) {
							uni.showModal({
								title: '',
								content: '请在系统设置中打开定位服务',
								showCancel: false,
								success: result => {
									if (result.confirm) {
										uni.navigateBack()
									}
								}
							})
						} else {
							this.showFailToast('授权失败')
						}
						reject();
					}
				})
			})
		},

		// 请求保存图片权限
		checkAuthorize(onlyCheck) {
			let that = this;
			return new Promise((resolve, reject) => {
				uni.getSetting({
					success(res) {
						let authSetting = res.authSetting;
						console.log("authSetting", authSetting);
						let status = authSetting["scope.album"];
						// 未开启权限弹窗提示获取
						if (status == false) {
							// 用户拒绝授权
							reject();
						} else if (
							status == null ||
							status == undefined ||
							JSON.stringify(status) == "{}"
						) {
							if (onlyCheck) {
								resolve()
								return;
							}
							// 用户为授权
							uni.authorize({
								scope: "scope.writePhotosAlbum",
								success() {
									resolve();
								},
								fail() {
									reject();
								},
							});
						} else {
							resolve();
						}
					},
				});
			});
		},

		// 清理系统缓存文件
		clearSysFile() {
			return new Promise((resolve) => {
				// 把文件删除后再写进，防止超过最大范围而无法写入
				const fsm = uni.getFileSystemManager();  //文件管理器
				fsm.getFileInfo({
					filePath: uni.env.USER_DATA_PATH,// 当时写入的文件夹
					success: file => {
						console.log('文件信息', file)
					},
					fail: err => {
						console.log('文件信息失败', err)
					}
				})
				fsm.getSavedFileList({
					filePath: uni.env.USER_DATA_PATH,// 当时写入的文件夹
					success: file => {
						console.log('本地缓存文件', file)
					},
					fail: err => {
						console.log('本地缓存文件失败', err)
					}
				})
				fsm.readdir({  // 获取文件列表
					dirPath: uni.env.USER_DATA_PATH,// 当时写入的文件夹
					success(res) {
						console.log('目录内文件列表', res)
						res.files.forEach((el) => { // 遍历文件列表里的数据
							// 删除存储的垃圾数据
							fsm.unlink({
								filePath: `${uni.env.USER_DATA_PATH}/${el}`, // 这里注意。文件夹也要加上，如果直接文件名的话会无法找到这个文件
								success: succ => {
									console.log('删除成功', succ)
								},
								fail(e) {
									console.log('readdir文件删除失败：', e)
								}
							});
						})
						resolve()
					},
					fail: err => {
						console.log('目录内文件列表失败', err)
					}
				})
			})
		},

		getPraivteLetterAuthAccount() {
			return new Promise((resolve, reject) => {
				let params = {
					shopId: this.getEnvStorage('shopId') || ''
				}
				vm.$u.api.getPraivteLetterAuthAccount(params).then(res => {
					if (res.code == 0) {
						resolve(res.result || [])
					} else {
						resolve([])
					}
				}).catch(err => {
					resolve([])
				})
			})
		},

		// 功能模块接口
		queryAppPermissions() {
			return new Promise((resolve, reject) => {
				let params = {
					shopId: this.getEnvStorage('shopId') || ''
				}
				if (!params.shopId) {
					return
				}
				vm.$u.api.queryAppPermissions(params).then(res => {
					if (res.code == 0 && res.result) {
						this.setEnvStorage('permissions', JSON.stringify(res.result.permissions || []))
						resolve()
					} else {
						this.setEnvStorage('permissions', JSON.stringify([]))
						resolve()
					}
				}).catch(err => {
					this.setEnvStorage('permissions', JSON.stringify([]))
					resolve()
				})
			})
		},


		/*
		 * 判断obj是否为一个整数 整数取整后还是等于自己。利用这个特性来判断是否是整数
		 */
		isInteger(obj) {
			// 或者使用 Number.isInteger()
			return Math.floor(obj) === obj
		},

		/*
		 * 将一个浮点数转成整数，返回整数和倍数。如 3.14 >> 314，倍数是 100
		 * @param floatNum {number} 小数
		 * @return {object}
		 *   {times:100, num: 314}
		 */
		toInteger(floatNum) {
			// 初始化数字与精度 times精度倍数  num转化后的整数
			var ret = {
				times: 1,
				num: 0
			}
			var isNegative = floatNum < 0 //是否是小数
			if (this.isInteger(floatNum)) { // 是否是整数
				ret.num = floatNum
				return ret //是整数直接返回
			}
			var strfi = floatNum + '' // 转换为字符串
			var dotPos = strfi.indexOf('.')
			var len = strfi.substr(dotPos + 1).length // 拿到小数点之后的位数
			var times = Math.pow(10, len) // 精度倍数
			/* 为什么加0.5?
				前面讲过乘法也会出现精度问题
				假设传入0.16344556此时倍数为100000000
				Math.abs(0.16344556) * 100000000=0.16344556*10000000=1634455.5999999999 
				少了0.0000000001
				加上0.5 0.16344556*10000000+0.5=1634456.0999999999 parseInt之后乘法的精度问题得以矫正
			*/
			var intNum = parseInt(Math.abs(floatNum) * times + 0.5, 10)
			ret.times = times
			if (isNegative) {
				intNum = -intNum
			}
			ret.num = intNum
			return ret
		},

		rpxTopx(rpx) {
			let deviceWidth = uni.getSystemInfoSync().windowWidth; //获取设备屏幕宽度
			let px = (deviceWidth / 750) * Number(rpx)
			return Math.floor(px);
		},

		pxTorpx(px) {
			let deviceWidth = wx.getSystemInfoSync().windowWidth; //获取设备屏幕宽度
			let rpx = (750 / deviceWidth) * Number(px)
			return Math.floor(rpx);
		},

		/*
		 * 核心方法，实现加减乘除运算，确保不丢失精度
		 * 思路：把小数放大为整数（乘），进行算术运算，再缩小为小数（除）
		 * @param a {number} 运算数1
		 * @param b {number} 运算数2
		 */
		operation(a, b, op) {
			var o1 = this.toInteger(a)
			var o2 = this.toInteger(b)
			var n1 = o1.num // 3.25+3.153
			var n2 = o2.num
			var t1 = o1.times
			var t2 = o2.times
			var max = t1 > t2 ? t1 : t2
			var result = null
			switch (op) {
				// 加减需要根据倍数关系来处理
				case 'add':
					if (t1 === t2) { // 两个小数倍数相同
						result = n1 + n2
					} else if (t1 > t2) {
						// o1 小数位 大于 o2
						result = n1 + n2 * (t1 / t2)
					} else { // o1小数位小于 o2
						result = n1 * (t2 / t1) + n2
					}
					return result / max
				case 'subtract':
					if (t1 === t2) {
						result = n1 - n2
					} else if (t1 > t2) {
						result = n1 - n2 * (t1 / t2)
					} else {
						result = n1 * (t2 / t1) - n2
					}
					return result / max
				case 'multiply':
					// 325*3153/(100*1000) 扩大100倍 ==>缩小100倍
					result = (n1 * n2) / (t1 * t2)
					return result
				case 'divide':
					// (325/3153)*(1000/100)  缩小100倍 ==>扩大100倍
					result = (n1 / n2) * (t2 / t1)
					return result
			}
		},

		// 编码解码页面长参数，避免&等符号导致的错误
		encodeQueryParam(param) {
			return encodeURIComponent(JSON.stringify(param));
		},
		decodeQueryParam(str) {
			let result = null;
			try {
				result = JSON.parse(decodeURIComponent(str));
			} catch (e) {
				console.log('util.js decodeQueryParam err', e)
			}
			return result;
		}
	};
}
export default {
	install
}
