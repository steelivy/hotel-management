export default{
  //是否阿里
  isAlipay: function () {
    // #ifdef H5
    let ua = window.navigator.userAgent.toLowerCase()
    if (ua.match(/Alipay/i) == 'alipay') {
      return true
    } else {
      return false
    }
    // #endif
    return uni.getSystemInfoSync().uniPlatform === 'mp-alipay'
  },
  //是否微信
  isWX: function () {
    // #ifdef H5
    let ua = window.navigator.userAgent.toLowerCase()
    if (ua.match(/MicroMessenger/i) == 'micromessenger') {
      return true
    } else {
      return false
    }
    // #endif
    // #ifdef MP-WEIXIN
      return true
    // #else
      return false
    // #endif
  },
  //判断是否在小程序
  isWxMini(){
    return new Promise((resolve, reject) => {
      // #ifdef MP
      resolve(true)
      // #endif
      // #ifndef MP
      resolve(false)
      // #endif

      // uni.getSystemInfo().then(res => {
      //   let uniPlatform = res[1].uniPlatform
      //   resolve(['mp-weixin', 'mp-alipay'].indexOf(uniPlatform) != -1)
      // })
    })
    
  	let _self = this;
    return new Promise((resolve,reject)=>{
      if(_self.isIOS()){//ios
        if(window.__wxjs_environment === 'miniprogram'){//小程序
          resolve(true);
        }else{
          resolve(false);
        }
      }else{
        if(jWeixin && _self.isWX()){
          jWeixin.miniProgram.getEnv(function(res) {
            resolve(res.miniprogram); // true
          })
        }else{
          resolve(false);
        }	  
      }
    })
  },
  /**
     * 判断是否在电脑端浏览器
     *
     * @return: {Boolean} true:  
     *                    false: 
     */
  isPC () {
    return navigator.userAgent.indexOf('Android') < 0 && navigator.userAgent.indexOf('iPhone') < 0 && navigator.userAgent.indexOf('iPod') < 0 && navigator.userAgent.indexOf('Symbian') < 0
  },
  /**
     * 判断是否在ios
     *
     * @return: {Boolean} true:  在ios中
     *                    false: 不在ios中
     */
  isIOS () {
    return uni.getSystemInfoSync().platform === 'ios'
    return !!navigator.userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/)
  },
  /**
     * 判断是否在ios9
     *
     * @return: {Boolean} true:  
     *                    false: 
     */
  isIOS9 () {
    return navigator.userAgent.indexOf('iPhone OS 9') > -1
  },
  /**
     * 判断是否在iPhoneX
     *
     * @return: {Boolean} true:  
     *                    false: 
     */
  iPhoneX () {
    if(/iphone/gi.test(window.navigator.userAgent)){
      /* iPhone X、iPhone XS */
      var x=(window.screen.width === 375 && window.screen.height === 812);
      /* iPhone XS Max */
      var xsMax=(window.screen.width === 414 && window.screen.height === 896);
      /* iPhone XR */
      var xR=(window.screen.width === 414 && window.screen.height === 896);
      if(x || xsMax || xR){
          return true;
      }else{
          return false;
      }
    }else{
        return false
    }
	  // alert($(document).height());
    // return navigator.userAgent.indexOf('iPhone OS') > -1 && (($(document).width() == 724 && $(document).height() == 375) || (($(document).width() == 375 && $(document).height() == 724)))
  },
  /**
   * 判断是否在android
   *
   * @return: {Boolean} true:  在android中
   *                    false: 不在android中
   */
  isAndroid () {
    return navigator.userAgent.indexOf('Android') > -1 || navigator.userAgent.indexOf('Adr') > -1
  },
  isHuawei (){
    const modal = ["HUAWEI","MHA-AL00","BLN-AL10","DUK-AL20","KLW-AL10","CAM-AL00","NEM-AL10","VTR-AL00","PRA-AL00","CAM-TL00","PLK-AL10","KIW-TL00","DIG-AL00","CUN-AL00","ATH-AL00","VKY-AL00","VIE-AL10"]
    let isHuawei = false;
    for(let i = 0;i<modal.length;i++){
      if(navigator.userAgent.indexOf(modal[i]) > -1){
        isHuawei = true;
        break;
      }
    }
    return isHuawei//navigator.userAgent.indexOf('HUAWEI') > -1 || navigator.userAgent.indexOf('ATH-AL00') > -1 
  },
  isLiuHaiAndroid (){
    const modal = ["INE-AL00"]
    let isLiuHaiAndroid = false;
    for(let i = 0;i<modal.length;i++){
      if(navigator.userAgent.indexOf(modal[i]) > -1){
        isLiuHaiAndroid = true;
        break;
      }
    }
    return isLiuHaiAndroid//navigator.userAgent.indexOf('HUAWEI') > -1 || navigator.userAgent.indexOf('ATH-AL00') > -1 
  },
  isIPhone6 () {
    const modal = ["A1586"," A1589","A1549"];
    let iPhone = false;
    for(let i = 0;i<modal.length;i++){
      if(navigator.userAgent.indexOf(modal[i]) > -1){
        iPhone = true;
        break;
      }
    }
    return iPhone
  },
  isIPhone6Plus () {
    const modal = ["A1524"," A1593", "A1522"];
    let iPhone = false;
    for(let i = 0;i<modal.length;i++){
      if(navigator.userAgent.indexOf(modal[i]) > -1){
        iPhone = true;
        break;
      }
    }
    return iPhone
  },
}