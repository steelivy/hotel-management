// 小程序获取openId
const getDyOpenIdUrl = '/api/mdj/xcxAuth'
// 首页接口
const homeUrl = '/api/mdj/query/home'
const addMobileUrl = '/api/mdj/getMobile'
const getLoginInfoUrl = '/api/mdj/userinfo'
const getRoomListUrl = '/api/mdj/query/roomlist'
const getCouponListUrl = '/api/mdj/query/couponList'
const receiveCouponUrl = '/api/mdj/coupon/receive'
const saveUserinfoUrl = '/api/mdj/save/userinfo'
const getMyCouponListUrl = '/api/mdj/query/getCouponList'
const getOrderListUrl = '/api/mdj/getOrderList'
const createReservationOrderUrl = '/api/mdj/createReservationOrder'
const getOrderAmountUrl = '/api/mdj/getOrderAmount'
const getMoreServiceUrl = '/api/mdj/getMoreService'
const getOrderAvailableCouponsUrl = '/api/mdj/getOrderAvailableCoupons'
const cancelOrderUrl = '/api/mdj/cancelOrder'
const getMemberPointsListUrl = '/api/mdj/getMemberPointsList'
const getMemberBenefitsUrl = '/api/mdj/getMemberBenefits'
const memberBenefitsUrl = '/api/mdj/my/memberBenefits'
const customCouponListUrl = '/api/mdj/query/customCouponDetails'
const customCouponReceiveUrl = '/api/mdj/customCouponReceive'
const writeOffUrl = '/api/mdj/writeOff'
const getOrderAvailableDelayCouponsUrl = '/api/mdj/getOrderAvailableDelayCoupons'
const modifyOrderUrl = '/api/mdj/modifyOrder'
const cancelOrderRefundUrl = '/api/mdj/cancelOrderRefund'

// H5早餐券相关接口
const h5CustomCouponDetailsUrl = '/api/query/customCouponDetails'
const h5ReceiveCouponUrl = '/api/coupon/receive'
const h5GetCouponListUrl = '/api/query/getCouponList'
const h5WriteOffUrl = '/api/coupon/writeOff'
const adminLoginUrl = '/api/coupon/login'
const h5GetWriteCntUrl = '/api/query/writeOff/cnt'

const startMarryUrl = 'https://zwms.gdbs.gov.cn/ebus/huazi_gdhy/hunyin/api/h5/mobile/marriage/create_reservation'
const getMarryUrl = 'https://zwms.gdbs.gov.cn/ebus/huazi_gdhy/hunyin/api/h5/mobile/common/list_reservations'

// const getCityUrl = 'https://zwms.gdbs.gov.cn/ebus/huazi_gdhy/hunyin/api/h5/mobile/common/list_areacodes'
const getMarryCityListUrl = 'https://yydj.doubleshadowswan.com:17305/get_address'
const getCityUrl = 'https://yydj.doubleshadowswan.com:17305/get_addresses'

const createReUrl = 'https://zsfy.gzsums.net/weixin/1/reservation/resourcedate/'
const jzxUrl = 'https://zsfy.gzsums.net/weixin/1/reservation'
const getDoctorUrl = 'https://zsfy.gzsums.net/weixin/1/reservation/department/02017/doctors?is_special=0&oper_type=1'


// 如果没有通过拦截器配置域名的话，可以在这里写上完整的URL(加上域名部分)
//分享接口
const getShareUrl = '/api/pub/wxshare'
// 上报门店接口
const shopVisitReportUrl = '/api/pub/shopVisitReport'
const getHotCarListUrl = '/api/pub/getHotCarList'
const getShopListUrl = '/api/pub/distanceShop/list'
// 经销商详情
const shopInfoUrl = '/api/pub/shop/info'
// 文章tab
const getTabListUrl = '/api/carArticle/getTabList'
// 文章分类列表
const articleListUrl = '/api/pub/article/list'
// 获取预约试驾背景图和车型选项
const getTestDriveInfoUrl = '/api/pub/getTestDriveInfo'
// 发短信验证码
const smsUrl = '/sys/sms'
// 试驾和获取底价提交接口
const prebookUrl = '/api/pub/prebook'
// 隐私政策接口
const getPrivacyUrl = '/api/pub/getPrivacy'
// 首页优惠车型接口
const getDiscountCarListUrl = '/api/pub/getDiscountCarList'
// 车型选择列表接口
const getCarListUrl = '/api/pub/getCarList'
// 车型详情接口
const getCarDetailUrl = '/api/pub/getCarDetail'
// 车型配置详情接口
const getCarConfigUrl = '/api/carParam/getCarDetail'
// 优惠活动详情接口
const getDiscountInfoUrl = '/api/signup/detail'
// 购车指南详情接口
const getGuideInfoUrl = '/api/pub/article/queryById'
// 今日销售接口
const getShopTodaySalesUrl = '/api/pub/getShopTodaySales'
// 首页右下角icon接口
const getShopIconListUrl = '/api/pub/getShopIconList'
// 优惠券单独接口
const getShopCouponListUrl = '/api/pub/getShopCouponList'

const getCouponReceivePageUrl = '/api/coupon/getCouponReceivePage'
const getRegisterReceivePageUrl = '/api/personal/getRegisterReceivePage'

const behaviorReportUrl = '/api/pub/behaviorReport'

const getCouponUrl = '/api/pub/getCoupon'

// 提交优惠活动报名
const signupAddUrl = '/api/signup/add'
// // 集客工具-验证码接口
// const toolSendsmsUrl = 'https://customertest.cloudstore.net.cn/routetool/api/tool/sendsms'
// // 集客工具-领优惠券接口
// const toolAddReceiveUrl = 'https://customertest.cloudstore.net.cn/routetool/api/tool/coupon/addReceive'
// // 集客工具-活动报名信息接口
// const toolQueryRegistrationUrl = 'https://customertest.cloudstore.net.cn/routetool/api/tool/registration/queryRegistration'
// // 集客工具-提交活动报名接口
// const toolRegisterUrl = 'https://customertest.cloudstore.net.cn/routetool/api/tool/registration/register'

// 集客工具-验证码接口
const toolSendsmsUrl = 'https://tools.cloudstore.net.cn/routetool/api/tool/sendsms'
// 集客工具-领优惠券接口
const toolAddReceiveUrl = 'https://tools.cloudstore.net.cn/routetool/api/tool/coupon/addReceive'
// 集客工具-活动报名信息接口
const toolQueryRegistrationUrl = 'https://tools.cloudstore.net.cn/routetool/api/tool/registration/queryRegistration'
// 集客工具-提交活动报名接口
const toolRegisterUrl = 'https://tools.cloudstore.net.cn/routetool/api/tool/registration/register'
// 集客工具-查询优惠券信息接口


// 抖音券-查询领券详情
const getCouponInfoUrl = '/api/personal/getCouponInfo'
// 抖音券-已领取抖音优惠券分页列表
const getDyCouponReceivePageUrl = '/api/personal/getDyCouponReceivePage'

// 查询车型报名留资配置
const queryCarSignUpByIdUrl = '/api/personal/queryCarSignUpById'
// 提交车型报名留资
const addCarSignUpUrl = '/api/personal/carSignUp/add'

// 优惠券领取接口
const addCouponUrl = '/api/coupon/add'

// 门店绑定主播账号及查看后台信息初始化接口
const getShopBindingDyUrl = '/api/pub/getShopBindingDy'
// 门店刷新绑定主播账号授权码接口
const dyRefreshBindQrcodeUrl = '/api/pub/dyRefreshBindQrcode'
// 门店授权绑定
const bindSelfMountUserUrl = '/api/pub/bindSelfMountUser'
// 服务商模式——获取相关权限信息
const getAppAbilitiesUrl = '/api/pub/getAppAbilities'

// 城市列表
const getCityListUrl = '/api/pub/getCityList'
// --START--营销任务相关接口
// 任务列表接口
const getLiveTasksUrl = '/api/pub/getLiveTasks'
// 任务详情接口
const getLiveTaskInfoUrl = '/api/pub/getLiveTaskInfo'
// 用户参与任务
const addLiveTaskUserUrl = '/api/common/addLiveTaskUser'
// --END--营销任务相关接口


// 私信配置接口
const getPraivteLetterAuthAccountUrl = '/api/pub/getPraivteLetterAuthAccount'

// 核销
const writeoffUrl = '/api/pub/writeoff'

// 小程序订阅模板
const getAppSubscribeTplUrl = '/api/pub/getAppSubscribeTpl'

// 直播预览流
const getAppAnchorListUrl = '/api/pub/getAppAnchorList'

// 意向购车城市开关
const getSwitchSetUrl = '/api/pub/getSwitchSet'

// 保存用户抖音号
const editDyIdUrl = '/api/personal/editDyId'

// 功能模块接口
const queryAppPermissionsUrl = '/api/pub/queryAppPermissions'

// --START--服务商城相关接口

// 服务列表接口
const getMallListUrl = '/api/mall/list'

// 服务详情接口
const getMallInfoUrl = '/api/mall/info'

// 提交订单接口
const createOrderUrl = '/api/mall/createOrder'

// 删除订单接口
const delOrderUrl = '/api/mall/delOrder'

// 查询订单接口
const getUserOrderListUrl = '/api/mall/getUserOrderList'

// 查询订单详情接口
const getOrderInfoUrl = '/api/mall/getOrderInfo'

// 查询订单支付token
const getPayTokenUrl = '/api/mall/getPayToken'

// 获取商品CombinId
const getCombinIdWithChildIdUrl = '/api/mall/getCombinIdWithChildId'

// --END--服务商城相关接口
// 字典查询接口
// 退款申请类型接口
const refundTypeUrl = '/api/pub/getDictItems/refundType'

// 退款申请原因接口
const refundReasonUrl = '/api/pub/getDictItems/refundReason'

// 退款初始化接口
const refundInitUrl = '/api/mall/refund/init'

// 退款申请提交接口
const refundUrl = '/api/mall/refund'

// 退款结果查询接口
const queryRefundUrl = '/api/mall/refund/query'

// 抽奖详情
const getTurnTableCouponInfoUrl = '/api/pub/getLotteryInfo'
// 开始抽奖
const drawLotteryUrl = '/api/pub/drawLottery'

// 地理位置上报
const uploadPositionUrl = 'api/pub/uploadPosition'


// 此处第二个参数vm，就是我们在页面使用的this，你可以通过vm获取vuex等操作，更多内容详见uView对拦截器的介绍部分：
const install = (Vue, vm) => {
	// // 示例
	// // 此处没有使用传入的params参数
	// const getSearch = (params = {}) => vm.$u.get(hotSearchUrl, {
	// 	id: 2
	// });
	// // 此处使用了传入的params参数，一切自定义即可
	// const getInfo = (params = {}) => vm.$u.post(indexUrl, params);
	// // 将各个定义的接口名称，统一放进对象挂载到vm.$u.api(因为vm就是this，也即this.$u.api)下
	// vm.$u.api = {getSearch, getInfo};


	/**
	 * 分享接口
	 * @param {object} params -请求参数对象
	 * @param {string} params.activityId -活动id
	 * @param {string} data.id -内容id
	 * @param {string} params.type -分享类型
	 * @param {string} params.shareurl -分享链接
	 * @param {string} data.source -渠道来源
	 * @param {string} params.openId -用户id
	 */
	const wxShare = (params = {}) => vm.$u.post(getShareUrl, params);

	/**
	 * 核验已绑定手机号接口
	 * @param {string} params.captcha -活动id
	 */
	const checkMobile = (params = {}) => vm.$u.get(checkMobileUrl, params);

	const shopVisitReport = (params = {}) => vm.$u.post(shopVisitReportUrl, {
		contentType: 'form',
		...params
	});


	const getHotCarList = (params = {}) => vm.$u.get(getHotCarListUrl, params);

	const getShopList = (params = {}) => vm.$u.get(getShopListUrl, params);

	/**
	 * 经销商接口
	 * @param {string} params.shopId -经销商id
	 */
	const shopInfo = (params = {}) => vm.$u.get(shopInfoUrl, params);

	/**
	 * 文章tab接口
	 */
	const getTabList = (params = {}) => vm.$u.get(getTabListUrl, params);

	/**
	 * 文章分类列表接口
	 * @param {string} params.type
	 * @param {string} params.pageNo
	 * @param {string} params.pageSize
	 */
	const articleList = (params = {}) => vm.$u.get(articleListUrl, params);

	/**
	 * 获取预约试驾背景图和车型选项接口
	 * @param {string} params.shopId -经销商id
	 */
	const getTestDriveInfo = (params = {}) => vm.$u.get(getTestDriveInfoUrl, params);

	/**
	 * 发短信验证码接口
	 * @param {string} params.phone
	 * @param {string} params.smsmode=0 
	 */
	const sms = (params = {}) => vm.$u.post(smsUrl, params);

	/**
	 * 试驾和获取底价提交接口
	 * @param {object} params.type -testDrive试驾，enquiry获取底价
	 * @param {string} params.mobile
	 * @param {string} params.name
	 * @param {string} params.captcha -验证码
	 * @param {string} params.shopId
	 * @param {string} params.carStyleId 
	 * @param {string} params.prebookTime 
	 * @param {string} params.openId -用户id
	 */
	const prebook = (params = {}) => vm.$u.post(prebookUrl, params);

	/**
	 * 隐私政策接口
	 * @param {string} params.shopId -经销商id
	 */
	const getPrivacy = (params = {}) => vm.$u.get(getPrivacyUrl, params);

	/**
	 * 首页优惠车型接口
	 * @param {string} params.shopId -经销商id
	 */
	const getDiscountCarList = (params = {}) => vm.$u.get(getDiscountCarListUrl, params);

	/**
	 * 车型选择列表接口
	 * @param {string} params.shopId -经销商id
	 */
	const getCarList = (params = {}) => vm.$u.get(getCarListUrl, params);

	/**
	 * 车型详情接口
	 * @param {string} params.shopId -经销商id
	 */
	const getCarDetail = (params = {}) => vm.$u.get(getCarDetailUrl, params);

	/**
	 * 车型配置详情接口
	 * @param {string} params.shopId -经销商id
	 */
	const getCarConfig = (params = {}) => vm.$u.get(getCarConfigUrl, params);

	/**
	 * 优惠活动详情接口
	 * @param {string} params.shopId -经销商id
	 */
	const getDiscountInfo = (params = {}) => vm.$u.get(getDiscountInfoUrl, params);

	/**
	 * 购车指南详情接口
	 * @param {string} params.shopId -经销商id
	 */
	const getGuideInfo = (params = {}) => vm.$u.get(getGuideInfoUrl, params);

	/**
	 * 获取今日销售接口
	 * @param {string} params.shopId -经销商id
	 */
	const getShopTodaySales = (params = {}) => vm.$u.get(getShopTodaySalesUrl, params);

	const getCouponReceivePage = (params = {}) => vm.$u.get(getCouponReceivePageUrl, params);
	/**
	 * 首页右下角icon接口
	 * @param {string} params.shopId -经销商id
	 */
	const getShopIconList = (params = {}) => vm.$u.get(getShopIconListUrl, params);
	/**
	 * 首页右下角icon接口
	 * @param {string} params.shopId -经销商id
	 */
	const getShopCouponList = (params = {}) => vm.$u.get(getShopCouponListUrl, params);

	/**
	 * 提交优惠活动报名
	 * @param {string} params.id
	 * @param {string} params.name
	 * @param {string} params.phone
	 * @param {string} params.smsCode
	 * @param {string} params.useId
	 */
	const signupAdd = (params = {}) => vm.$u.post(signupAddUrl, params);


	const getRegisterReceivePage = (params = {}) => vm.$u.get(getRegisterReceivePageUrl, params);

	/**
	 * 集客工具验证码
	 * @param {string} params.id
	 * @param {string} params.businessId 7381b2643c4f3686376ee2f61c3713b2
	 * @param {string} params.shopId
	 * @param {string} params.type
	 * @param {string} params.source
	 * @param {string} params.openId
	 * @param {string} params.phone
	 */
	const toolSendsms = (params = {}) => vm.$u.get(toolSendsmsUrl, params);

	/**
	 * 集客工具领优惠券
	 * @param {string} params.id
	 * @param {string} params.businessId 7381b2643c4f3686376ee2f61c3713b2
	 * @param {string} params.shopId
	 * @param {string} params.type
	 * @param {string} params.source
	 * @param {string} params.openId
	 * @param {string} params.phone
	 * @param {string} params.name
	 * @param {string} params.code
	 */
	const toolAddReceive = (params = {}) => vm.$u.post(toolAddReceiveUrl, params);

	/**
	 * 集客工具活动报名信息
	 * @param {string} params.id
	 * @param {string} params.businessId 7381b2643c4f3686376ee2f61c3713b2
	 * @param {string} params.shopId
	 * @param {string} params.type
	 * @param {string} params.openId
	 */
	const toolQueryRegistration = (params = {}) => vm.$u.get(toolQueryRegistrationUrl, params);
	const getCoupon = (params = {}) => vm.$u.get(getCouponUrl, params);

	/**
	 * 集客工具提交活动报名
	 * @param {string} params.id
	 * @param {string} params.businessId 7381b2643c4f3686376ee2f61c3713b2
	 * @param {string} params.shopId
	 * @param {string} params.type
	 * @param {string} params.appointmentTime
	 * @param {string} params.openId
	 * @param {string} params.phone
	 * @param {string} params.name
	 * @param {string} params.code
	 */
	const toolRegister = (params = {}) => vm.$u.post(toolRegisterUrl, params);
	const behaviorReport = (params = {}) => vm.$u.post(behaviorReportUrl, params);

	/**
	 * 查询领券详情
	 */
	const getCouponInfo = (params = {}) => vm.$u.get(getCouponInfoUrl, params);

	/**
	 * 已领取抖音优惠券分页列表
	 */
	const getDyCouponReceivePage = (params = {}) => vm.$u.get(getDyCouponReceivePageUrl, params);


	/**
	 * 查询车型报名留资配置
	 */
	const queryCarSignUpById = (params = {}) => vm.$u.get(queryCarSignUpByIdUrl, params);

	/**
	 * 提交车型报名留资
	 */
	const addCarSignUp = (params = {}) => vm.$u.post(addCarSignUpUrl, params);


	/**
	 * 优惠券领取接口
	 */
	const addCoupon = (params = {}) => vm.$u.post(addCouponUrl, params);


	/**
	 * 门店绑定主播账号及查看后台信息初始化接口
	 */
	const getShopBindingDy = (params = {}) => vm.$u.get(getShopBindingDyUrl, params);


	/**
	 * // 门店刷新绑定主播账号授权码接口
	 */
	const dyRefreshBindQrcode = (params = {}) => vm.$u.get(dyRefreshBindQrcodeUrl, params);

	/**
	 * // 门店授权绑定
	 */
	const bindSelfMountUser = (params = {}) => vm.$u.post(bindSelfMountUserUrl, params);

	/**
	 * // 服务商模式——获取各种权限信息
	 */
	const getAppAbilities = (params = {}) => vm.$u.get(getAppAbilitiesUrl, params);

	// --START--营销任务相关接口

	/**
	 * // 任务列表接口
	 */
	const getLiveTasks = (params = {}) => vm.$u.get(getLiveTasksUrl, params);
	/**
	 * // 任务详情接口
	 */
	const getLiveTaskInfo = (params = {}) => vm.$u.get(getLiveTaskInfoUrl, params);
	/**
	 * // 任务上报接口
	 */
	const addLiveTaskUser = (params = {}) => vm.$u.post(addLiveTaskUserUrl, {
		contentType: 'form',
		...params
	});

	// --END--营销任务相关接口

	/**
	 * // 城市列表
	 */
	const getCityList = (params = {}) => vm.$u.get(getCityListUrl, params);

	/**
	 * // 核销
	 */
	const writeoff = (params = {}) => vm.$u.post(writeoffUrl, {
		contentType: 'form',
		...params
	});

	/**
	 * // 小程序订阅模板
	 */
	const getAppSubscribeTpl = (params = {}) => vm.$u.get(getAppSubscribeTplUrl, params);


	/**
	 * // 直播预览流
	 */
	const getAppAnchorList = (params = {}) => vm.$u.get(getAppAnchorListUrl, params);


	/**
	 * // 意向购车城市开关
	 */
	const getSwitchSet = (params = {}) => vm.$u.get(getSwitchSetUrl, params);

	/**
	 * // 私信配置
	 */
	const getPraivteLetterAuthAccount = (params = {}) => vm.$u.get(getPraivteLetterAuthAccountUrl, params);

	/**
	 * // 保存用户抖音号
	 */
	const editDyId = (params = {}) => vm.$u.post(editDyIdUrl, params);

	/**
	 * // 功能模块接口
	 */
	const queryAppPermissions = (params = {}) => vm.$u.get(queryAppPermissionsUrl, params);

	/**
	 * // 服务列表接口
	 */
	const getMallList = (params = {}) => vm.$u.get(getMallListUrl, params);

	/**
	 * // 服务详情接口
	 */
	const getMallInfo = (params = {}) => vm.$u.get(getMallInfoUrl, params);

	/**
	 * // 提交订单接口
	 */
	const createOrder = (params = {}) => vm.$u.post(createOrderUrl, params);

	/**
	 * // 删除订单接口
	 */
	const delOrder = (params = {}) => vm.$u.post(delOrderUrl, params);

	/**
	 * // 查询订单接口
	 */
	const getUserOrderList = (params = {}) => vm.$u.get(getUserOrderListUrl, params);

	/**
	 * // 查询订单详情接口
	 */
	const getOrderInfo = (params = {}) => vm.$u.get(getOrderInfoUrl, params);

	/**
	 * // 查询订单支付token
	 */
	const getPayToken = (params = {}) => vm.$u.get(getPayTokenUrl, params);

	/**
	* 退款申请类型接口
	*/
	const getRefundType = (params = {}) => vm.$u.get(refundTypeUrl, params);

	/**
	* 退款申请原因接口
	*/
	const getRefundReason = (params = {}) => vm.$u.get(refundReasonUrl, params);

	/**
	* 退款初始化接口
	*/
	const refundInit = (params = {}) => vm.$u.get(refundInitUrl, params);

	/**
	* 退款申请提交接口
	*/
	const refund = (params = {}) => vm.$u.post(refundUrl, params);

	/**
	* 退款结果查询接口
	*/
	const queryRefund = (params = {}) => vm.$u.get(queryRefundUrl, params);

	/**
	* 获取商品CombinId
	*/
	const getCombinIdWithChildId = (params = {}) => vm.$u.get(getCombinIdWithChildIdUrl, params);

	/**
	* 抽奖详情
	*/
	const getTurnTableCouponInfo = (params = {}) => vm.$u.get(getTurnTableCouponInfoUrl, params);
	/**
	* 开奖
	*/
	const drawLottery = (params = {}) => vm.$u.post(drawLotteryUrl, params);

	/**
	* 地理位置上报
	*/
	const uploadPosition = (params = {}) => vm.$u.post(uploadPositionUrl, {
		contentType: 'form',
		...params
	});


	// 酒店管理相关接口START
	const getDyOpenId = (params = {}) => vm.$u.get(getDyOpenIdUrl, params);
	/**
	 * 首页接口
	 */
	const home = (params = {}) => vm.$u.get(homeUrl, params);
	const addMobile = (params = {}) => vm.$u.post(addMobileUrl, {
		contentType: 'form',
		...params
	});
	const getLoginInfo = (params = {}) => vm.$u.post(getLoginInfoUrl, params);
	const getRoomList = (params = {}) => vm.$u.get(getRoomListUrl, params);
	const getCouponList = (params = {}) => vm.$u.get(getCouponListUrl, params);
	const receiveCoupon = (params = {}) => vm.$u.post(receiveCouponUrl, {
		contentType: 'form',
		...params
	});
	const saveUserInfo = (params = {}) => vm.$u.post(saveUserinfoUrl, params);
	const getMyCouponList = (params = {}) => vm.$u.get(getMyCouponListUrl, params);
	const getOrderList = (params = {}) => vm.$u.get(getOrderListUrl, params);
	const createReservationOrder = (params = {}) => vm.$u.post(createReservationOrderUrl, params);
	const getMoreService = (params = {}) => vm.$u.get(getMoreServiceUrl, params);
	const getOrderAmount = (params = {}) => vm.$u.get(getOrderAmountUrl, params);
	const getOrderAvailableCoupons = (params = {}) => vm.$u.get(getOrderAvailableCouponsUrl, params);
	const cancelOrder = (params = {}) => vm.$u.post(cancelOrderUrl, {
		contentType: 'form',
		...params
	});
	const getMemberPointsList = (params = {}) => vm.$u.get(getMemberPointsListUrl, params);
	const getMemberBenefits = (params = {}) => vm.$u.get(getMemberBenefitsUrl, params);
	const memberBenefits = (params = {}) => vm.$u.get(memberBenefitsUrl, params);
	const customCouponList = (params = {}) => vm.$u.get(customCouponListUrl, params);
	const customCouponReceive = (params = {}) => vm.$u.post(customCouponReceiveUrl, {
		contentType: 'form',
		...params
	});
	const writeOff = (params = {}) => vm.$u.post(writeOffUrl, {
		contentType: 'form',
		...params
	});
	const getOrderAvailableDelayCoupons = (params = {}) => vm.$u.get(getOrderAvailableDelayCouponsUrl, params);
	const modifyOrder = (params = {}) => vm.$u.post(modifyOrderUrl, {
		contentType: 'form',
		...params
	});
	const cancelOrderRefund = (params = {}) => vm.$u.post(cancelOrderRefundUrl, {
		contentType: 'form',
		...params
	});

	const h5CustomCouponDetails = (params = {}) => vm.$u.get(h5CustomCouponDetailsUrl, params);
	const h5ReceiveCoupon = (params = {}) => vm.$u.post(h5ReceiveCouponUrl, {
		contentType: 'form',
		...params
	});
	const h5GetCouponList = (params = {}) => vm.$u.get(h5GetCouponListUrl, params);
	const h5WriteOff = (params = {}) => vm.$u.post(h5WriteOffUrl, {
		contentType: 'form',
		...params
	});
	const adminLogin = (params = {}) => vm.$u.post(adminLoginUrl, {
		contentType: 'form',
		...params
	});
	const h5GetWriteCnt = (params = {}) => vm.$u.get(h5GetWriteCntUrl, params);
	
	
	
	
	// const customCouponReceive = (params = {}) => vm.$u.post(customCouponReceiveUrl, params);
	

	
	
	
	
	
	const startMarry = (params = {}) => vm.$u.post(startMarryUrl, params);
	const getMarry = (params = {}) => vm.$u.post(getMarryUrl, params);
	const getMarryCityList = (params = {}) => vm.$u.post(getMarryCityListUrl, params);
	const getCity = (params = {}) => vm.$u.post(getCityUrl, params);

	const jzx = (params = {}) => vm.$u.post(jzxUrl, {
		contentType: 'form',
		...params
	});

	const getDoctor = (params = {}) => vm.$u.get(getDoctorUrl, params);
	const createRe = (date, params = {}) => vm.$u.get(`${createReUrl}${date}`, params);















	// 酒店管理相关接口END


	// 将各个定义的接口名称，统一放进对象挂载到vm.$u.api(因为vm就是this，也即this.$u.api)下
	vm.$u.api = {
		getDyOpenId,
		home,
		addMobile,
		getLoginInfo,
		getRoomList,
		getCouponList,
		receiveCoupon,
		saveUserInfo,
		getMyCouponList,
		getOrderList,
		createReservationOrder,
		getMoreService,
		getOrderAmount,
		getOrderAvailableCoupons,
		cancelOrder,
		getMemberPointsList,
		getMemberBenefits,
		memberBenefits,
		customCouponList,
		customCouponReceive,
		writeOff,
		getOrderAvailableDelayCoupons,
		modifyOrder,
		cancelOrderRefund,

		h5CustomCouponDetails,
		h5GetCouponList,
		h5ReceiveCoupon,
		h5WriteOff,
		adminLogin,
		h5GetWriteCnt,

		startMarry,
		getMarry,
		getCity,
		getMarryCityList,

		jzx,
		getDoctor,
		createRe,


		wxShare,
		getHotCarList,
		getShopList,
		shopInfo,
		getTabList,
		articleList,
		getTestDriveInfo,
		sms,
		prebook,
		getPrivacy,
		getDiscountCarList,
		getCarList,
		getCarDetail,
		getCarConfig,
		getDiscountInfo,
		getGuideInfo,
		getShopTodaySales,
		getCouponReceivePage,
		getShopIconList,
		getShopCouponList,
		signupAdd,
		getRegisterReceivePage,
		toolSendsms,
		toolAddReceive,
		toolQueryRegistration,
		getCoupon,
		toolRegister,
		behaviorReport,
		shopVisitReport,
		getDyCouponReceivePage,
		getCouponInfo,
		queryCarSignUpById,
		addCarSignUp,
		addCoupon,
		getShopBindingDy,
		dyRefreshBindQrcode,
		bindSelfMountUser,
		getAppAbilities,
		getCityList,
		getLiveTasks,
		getLiveTaskInfo,
		addLiveTaskUser,
		writeoff,
		getAppSubscribeTpl,
		getAppAnchorList,
		getSwitchSet,
		getPraivteLetterAuthAccount,
		editDyId,
		queryAppPermissions,
		getMallList,
		getMallInfo,
		createOrder,
		delOrder,
		getUserOrderList,
		getOrderInfo,
		getPayToken,
		getRefundType,
		getRefundReason,
		refundInit,
		refund,
		queryRefund,
		getCombinIdWithChildId,
		getTurnTableCouponInfo,
		drawLottery,
		uploadPosition
	};
}

export default {
	install
}