// 这里的vm，就是我们在vue文件里面的this，所以我们能在这里获取vuex的变量，比如存放在里面的token
// 同时，我们也可以在此使用getApp().globalData，如果你把token放在getApp().globalData的话，也是可以使用的
var CONFIG_ENV = require("utils/config/env.js");
var origin = "";
if (CONFIG_ENV.env.name === "development") {
	origin = `https://xcx.meidaju.cn/route`;
	//   origin = `http://192.168.0.174:8095/routeydd`;
} else if (CONFIG_ENV.env.name === "production") {
	origin = `https://xcx.meidaju.cn/route`
}
let timer = null
let timerCnt = 0

const install = (Vue, vm) => {
	Vue.prototype.$u.http.setConfig({
		baseUrl: origin,
		// 如果将此值设置为true，拦截回调中将会返回服务端返回的所有数据response，而不是response.data
		// 设置为true后，就需要在this.$u.http.interceptor.response进行多一次的判断，请打印查看具体值
		// originalData: true,
		// 设置自定义头部content-type
		header: {
			'content-type': "application/json", //必须
		},
		// async:false
	});
	// 请求拦截，配置Token等参数
	Vue.prototype.$u.http.interceptor.request = (config) => {
		//动态携带链接中未知（需求）参数给后台
		// config.data = Object.assign({},vm.vuex_urlParams,config.data);
		//过滤参数为空的属性
		for (let k in config.data) {
			if (config.data[k] === '' || config.data[k] === undefined || config.data[k] ===
				null) { //null === ''(true)
				delete config.data[k];
			}
		}
		//签名
		// #ifndef H5
		config.data.timestamp = new Date().getTime();
		// #endif
		config.data.random = Math.random() * 100000000000000000;
		config.data.appId = CONFIG_ENV.env.appId
		config.data.minVersion = getApp().globalData.version
		config.data.customOpenId = getApp().globalData.openId
		config.data.openId = getApp().globalData.openId || vm.$store.state.vuex_token
		// #ifdef H5
		let ua = window.navigator.userAgent.toLowerCase()
		if (ua.match(/MicroMessenger/i) == 'micromessenger') {
			config.data.comeFrom = 'wxH5'
		} else {
			config.data.comeFrom = 'h5'
		}
		// #endif
		// #ifdef MP-WEIXIN
		config.data.comeFrom = 'wxMin'
		// #endif

		// let sign = vm.$u.common.getSign(config.data);
		// config.data.sign = sign;
		let token = vm.$u.common.getEnvStorage('token')
		config.header['X-Access-Token'] = token || ''
		if (config.method == 'GET' || config.data.contentType == 'form') {
			config.header['content-type'] = "application/x-www-form-urlencoded"
		}
		config.header['x-tif-did'] = '20ec2727-853d-19e6-ad63-c21686286cf7'
		config.header['x-tif-sid'] = '15038bbe3e2d5b5a39e624ca04f214509d'
		// config.header['Cookie'] = 'session=.eJxljstyokAYRt-l13YVl24u7gjREU0PGIKKG6ptWgHl2hCMqayzzWbmKfIKeZzkOcKkZpGpWfybU-f_6jyCqOJNTgtetGDcNh0fAcoYFyJqyyMvwBgYSnSTV1Bo8-Rga8deRk4iLSRnWV_lqIS9g6w-sCyLHLVr2_9J5hfWuSG_MzNFPmDmz_Tp2VwZgatnvR1IKsORatqH0D8v6Ok-3dQeNnd-7ZlhmVwbYARSEYmHgoGxPAJlxYs0HirKgzZRukk5bZMgpspx8oOzKIntjTOYI9DwfcNF8j16Xc1Wl3ofNCbnSDShS2kezOwNcQu4zCrHCsWWQoJv15ut2WUu6UksujXZRTf7O6_La6FO4speIdjuUgirXPUeVtxP5_sp8i7Ttl_q9jZbkEwhXwEn2qb3PPpTK-uygSX9Gy1ozoes95dfH7-f399eh49O8OZf-4sMl9LT4EqGRpnOKOSSrkLENAQNGcVwxzDWBkSxgf_O_Lf-9AmxUp6E.ZqPzrg.PK66pOoi8WwbRG1Xeq2wiqULvPs; Expires=Mon, 26-Aug-2024 19:06:22 GMT; HttpOnly; Path=/'

		// 方式一，存放在vuex的token，假设使用了uView封装的vuex方式，见：https://uviewui.com/components/globalVariable.html
		// config.header.token = vm.token;

		// 方式二，如果没有使用uView封装的vuex方法，那么需要使用$store.state获取
		// config.header.token = vm.$store.state.token;

		// 方式三，如果token放在了globalData，通过getApp().globalData获取
		// config.header.token = getApp().globalData.username;

		// 方式四，如果token放在了Storage本地存储中，拦截是每次请求都执行的，所以哪怕您重新登录修改了Storage，下一次的请求将会是最新值
		// const token = vm.$u.common.getEnvStorage('token');
		// config.header.token = token;
		if (!config.data.hideLoading) {
			if (!timer) {
				uni.showLoading({
					title: '请求中..',
					mask: true
				})
				timer = setTimeout(() => {
					if (timer) {
						uni.hideLoading()
						clearTimeout(timer)
						timer = null
					}
				}, 10000);
			} else {
				timerCnt++
			}
		}
		return config;
	}
	// 响应拦截，判断状态码是否通过
	Vue.prototype.$u.http.interceptor.response = (res) => {
		if (timer) {
			if (timerCnt == 0) {
				uni.hideLoading()
				clearTimeout(timer)
				timer = null
			} else {
				timerCnt--
			}
		}
		// 如果把originalData设置为了true，这里得到将会是服务器返回的所有的原始数据
		// 判断可能变成了res.statueCode，或者res.data.code之类的，请打印查看结果
		if (res.code == 0 || res.code == 200 || res.code == 1) {
			// 如果把originalData设置为了true，这里return回什么，this.$u.post的then回调中就会得到什么
			// return res.result || res.data;
			if (res.code == 1 && res.message.indexOf('Token失效') != -1) {
				uni.showToast({
					icon: 'none',
					title: '登录过期，请重新登录'
				})
				vm.$u.common.clearLoginInfo()
				setTimeout(() => {
					uni.redirectTo({
						url: '/pages/login/login'
					})
				}, 1000)
			}
			return res;
		} else if (res.code > 300 && res.code < 500) {
			vm.$u.vuex('toastData', {
				title: '服务器异常请稍后重试',
				duration: 2000,
			})
			return false;
		} else {
			return res;
		}
	}
}

export default {
	install
}
