export default {
	name: '',
	props:[],
	components: {},
	options: {
		// 微信小程序中 options 选项
		//multipleSlots: true, //  在组件定义时的选项中启动多slot支持，默认启用
		//styleIsolation: 'shared',  //  启动样式隔离。当使用页面自定义组件，希望父组件影响子组件样式时可能需要配置。具体配置选项参见：微信小程序自定义组件的样式
		//addGlobalClass: true, //  表示页面样式将影响到自定义组件，但自定义组件中指定的样式不会影响页面。这个选项等价于设置 styleIsolation: apply-shared
		//virtualHost: true,  //  将自定义节点设置成虚拟的，更加接近Vue组件的表现。我们不希望自定义组件的这个节点本身可以设置样式、响应 flex 布局等，而是希望自定义组件内部的第一层节点能够响应 flex 布局或者样式由自定义组件本身完全决定
	},
	data() {
		//这里存放数据
		return {
			
		};
	},
	//监听属性 类似于data概念
	computed: {},
	//监控data中的数据变化
	watch: {
	},
	//方法集合
	methods: {
		/**
		 * 获取缓存用户表单信息
		 * @param {Object} options
		 */
		getLocalStorageUserInfo(options) {
			let formInfo = options.act[this.vuex_storage];
			if (formInfo) {
				for (let k in formInfo) {
					this.finalDynamicForm.map(item => {
						if (item.type == k) {
							if (item.formType === 'input') {
								item.defaultValue = formInfo[k]
							} else if (item.formType === 'select') {
								let arr = [],
									placeholder;
								formInfo[k].map((d, i) => {
									if (i === 0) placeholder = `${d.label}`
									else placeholder += ` ${d.label}`
									arr.push(d.index)
								})
								item.placeholder = placeholder
								item.defaultValue = arr
								item.defaultId = formInfo[k][formInfo[k].length - 1].value
							}else if (item.formType === 'radio') {
								item.defaultValue = formInfo[k]
							}else if (item.formType === 'checkbox') {
								item.defaultValue = formInfo[k]
							}
						}
					})
				}
			}
		},
		/**
		 * 接口数据和预设数据融合
		 * @param {Object} d 预设item
		 * @param {Object} item 接口item
		 */
		formFormatData(item,d){
			if (item.formType) d.formType = item.formType;
			// if(item.cityList) d.list = item.cityList;
			if(item.placeHolder) d.placeholder = item.placeHolder;
			if (item.id) d.id = item.id;
			if (item.sceneType) d.sceneType = item.sceneType;
			if (item.type) d.type = item.type;
			
			if (item.graphicVerification) d.graphicVerification = item
				.graphicVerification;
			if (item.inputLength) d.inputLength = item.inputLength;
			if (item.regexpstr) d.regexp = item.regexpstr;
			if (item.inputStyle) d.inputStyle = item.inputStyle
			if (item.placeholderStyle) d.placeholderStyle = item
				.placeholderStyle;
			if (item.errMsg) d.errMsg = item.errMsg;
			if (item.defaultValue) d.defaultValue = item.defaultValue;
			
			if (item.selectList && Object.prototype.toString.call(item
					.selectList) === '[object Array]') {
				item.selectList.map(selectItem => {
					selectItem.value = selectItem.id;
					selectItem.label = selectItem.name;
				})
				d.list = item.selectList;
			}
			//城市
			if (item.cityList && Object.prototype.toString.call(item
					.cityList) === '[object Array]') {
				item.cityList.map(selectItem => {
					selectItem.value = selectItem.id;
					selectItem.label = selectItem.name;
				})
				d.list = item.cityList;
			}
			return d
		},
		/**
		 * 获取门店表单接口
		 */
		getShopListFormDataApi(params) {
			return this.$u.api.getShopListFormData(params).then(res => {
				res.map(selectItem => {
					selectItem.value = selectItem.id;
					selectItem.label = selectItem.name;
				})
				return this.$u.common.matchArrObjectItems(this.finalDynamicForm,[
					{
						key: 'type',
						value: 'shop',
					}
				]).then(d=>{
					d.shop.list = res;
					return d.shop
				})
			})
		},
		/**
		 * 表单项选择回调
		 * @param {Object} item -表单项数据
		 * @param {Object} e -变化数据
		 */
		formItemCallback(item,e){
			if(item.formType === 'select'){
				let arr = [],text = [];
				e.map((d, i) => {
					text.push(d.label)
					arr.push(d.index)
				})
				item.placeholder = text.join(' ')
				item.defaultValue = arr
			}
			//多个下拉列表之间存在数据前后关系,通过children判断关联子下拉
			if(item.type === 'city' && item.children){
				item.children.map(d=>{
					switch(d){
						case 'shop':
							let cityId = e[e.length-1].value
							this.wrapGetShopListFormDataApi({cityId}).then((res)=>{
								res.defaultValue = ''; //重置默认值
								res.defaultId = ''; //重置默认值对应id
								res.placeholder = '请选择城市'; //重置提示文案
								
								//重置门店缓存
								delete this.vuex_form_info.act[this.vuex_storage][res.type]
								let tmp = uni.getStorageSync('lifeData');
								tmp.vuex_form_info = this.vuex_form_info;
								this.$u.common.setStorage('lifeData',tmp);
							});
						break;
						default:
						break;
					}
				})
			}
		}
		
	},
	created() { //在实例创建完成后被立即调用
	
	},
	mounted() { //挂载到实例上去之后调用。详见 注意：此处并不能确定子组件被全部挂载，如果需要子组件完全挂载之后在执行操作可以使用nextTick
	
	},
	beforeCreate() {}, //在实例初始化之后被调用
	beforeMount() {}, //在挂载开始之前被调用
	beforeUpdate() {}, //数据更新时调用，发生在虚拟 DOM 打补丁之前
	updated() {}, //由于数据更改导致的虚拟 DOM 重新渲染和打补丁，在这之后会调用该钩子
	beforeDestroy() {}, //实例销毁之前调用。在这一步，实例仍然完全可用
	destroyed() {}, //Vue 实例销毁后调用。调用后，Vue 实例指示的所有东西都会解绑定，所有的事件监听器会被移除，所有的子实例也会被销毁
}