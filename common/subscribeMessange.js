export default {
  actid: '',
  diffTime: 0,
  callBackFlag: false,
  callBackSubscribe: null,//消息订阅回调方法
  //订阅消息
  init(params, callback) {
    let _self = this
    _self.callBackSubscribe = callback
    _self.requestSubscribeMessage(params.tmplIds)
  },
  requestSubscribeMessage(tmplIds) {
    let _self = this
    this.canIUseStorage('requestSubscribeMessage', (bool) => {
      if (!bool) {
        wx.showModal({
          title: '提示',
          content: '请更新微信，获取更好的体验',
          success() {
          }
        })
      } else {
        wx.requestSubscribeMessage({
          tmplIds: tmplIds,
          success(requestRes) {
            _self.checkSubscribeStatus().then(checkRes => {
              if (_self.callBackSubscribe) _self.callBackSubscribe(checkRes, requestRes)
              _self.callBackSubscribe = null
            }).catch(err => {
              if (_self.callBackSubscribe) _self.callBackSubscribe(null, requestRes)
              _self.callBackSubscribe = null
            })
          },
          fail(err) {
            _self.checkSubscribeStatus().then(res => {
              if (_self.callBackSubscribe) _self.callBackSubscribe(res)
              _self.callBackSubscribe = null
            }).catch(err => {
              if (_self.callBackSubscribe) _self.callBackSubscribe()
              _self.callBackSubscribe = null
            })
            console.log(err, 'err')
          }
        })
      }
    })
  },
  showSubscribeMessageTips() {//拒绝提示
    let _self = this
    wx.showModal({
      title: '提示',
      content: '开启订阅消息，获取更好的体验',
      success(res) {
        if (res.confirm) {//用户点击确定
          wx.openSetting({
            success(res) {
              _self.requestSubscribeMessage()
            }
          })
        } else if (res.cancel) {//用户点击取消
        }
      },
      fail() {
      }
    })
  },
  storageSubscribeMessage(params, callback) {//同意或拒绝提示时缓存订阅id，用于判断只弹一次
    let _self = this

    //同步缓存
    var value = wx.getStorageSync(params + _self.actid)
    if (value) {
      if (Date.now() - value.time > _self.diffTime) {//大于设定时间则重新请求
        wx.removeStorageSync(params + _self.actid)//超时则删除再缓存
        callback && callback(true)
      } else {
        callback && callback(false)
      }
    } else {
      wx.setStorageSync(params + _self.actid, {
        bool: true,
        time: Date.now()
      })
      callback && callback(true)
    }
  },
  canIUseStorage(value, callback) {//检测api是否可用
    if (!wx.canIUse(value)) {//不可用时提示一次
      wx.getStorage({
        key: 'canIUse',
        success(res) {
          let arr = res.data
          if (!arr.includes(value)) {//没有存储
            arr.push(value)
            wx.setStorage({
              key: 'canIUse',
              data: arr,
              success() {
                callback && callback()
              }
            })
          } else {//已存储
          }
        },
        fail(err) {
          let arr = [value]
          wx.setStorage({
            key: 'canIUse',
            data: arr,
            success() {
              callback && callback()
            }
          })
        }
      })
    } else {//可以用
      callback && callback(true)
      return true
    }
  },
  // 获得消息订阅状态
  checkSubscribeStatus() {
    // 注意！2.10.0以上才支持返回订阅消息
    return new Promise((resolve, reject) => {
      wx.getSetting({
        withSubscriptions: true,
        success: res => {
          resolve(res)
        },
        fail: err => {
          reject(err)
        }
      })
    })
  }
}