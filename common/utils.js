export default {
  queryParam(name) {
    // var svalue = window.location.hash.match(new RegExp('[\?\&]' + name + '=([^\&]*)(\&?)', 'i'))
    // return decodeURIComponent(svalue ? svalue[1] : svalue)
    //vue路由非history模式兼容
    var u;
    if (window.location.href.indexOf("#") > -1 && window.location.href.indexOf("#") < window.location.href.indexOf("?")) {
      var tmpArr = window.location.href.split("?");
      if (tmpArr.length > 1) {
        u = tmpArr[1].replace('&amp;', '&')
      } else {
        return null;
      }
    } else {
      u = window.location.search.replace('&amp;', '&')
    }

    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)')
    var r = u.substr(u.indexOf('\?') + 1).match(reg)
    return r != null ? r[2] : ''
  },
  //去掉空格
  trim(str) {
    return str.replace(/(^\s*)|(\s*$)/g, "");
  },
  /** 过滤链接参数
   * @param {Object} 过滤字段
   * @param {Object} 链接
   */
  filterParam(d, l) {
    if (Object.prototype.toString.call(d) == "[object Array]") {
      var endUrl = l
      for (var i in d) {
        endUrl = del(d[i], endUrl)
      }
      return endUrl
    } else {
      return del(d, l);
    }
    function del(n, l = window.location.href) {
      let urlSplit = l.split("?");
      if (!urlSplit[1]) return urlSplit[0];
      let arr = urlSplit[1].split('&')
      var param = arr.filter((item) => {
        if (item.indexOf(n) == -1) return item
      })
      param = param.join("&")
      let url = urlSplit[0] + "?" + param
      return url
    }
  },

  /**
   * 匹配多个数组对象
   * @param {Array} arr 数组对象
   * @param {Array} itemList item列表
   */
  matchArrObjectItems(arr, itemList) {
    return new Promise(async (resolve, reject) => {
      let resultObj = null;
      await itemList.map(async res => {
        await this.matchArrObjectItem(arr, res.key, res.value).then(d => {
          resultObj = Object.assign({}, resultObj, { [res.value]: d })
        })
      })
      if (resultObj) {
        resolve(resultObj)
      } else {
        reject('no mach item')
      }
    })
  },

  // 小程序自动更新
  checkUpdateVersion() {
    //判断微信版本是否 兼容小程序更新机制API的使用
    if (wx.canIUse('getUpdateManager')) {
      //创建 UpdateManager 实例
      const updateManager = wx.getUpdateManager();
      //检测版本更新
      updateManager.onCheckForUpdate(function (res) {
        // 请求完新版本信息的回调
        if (res.hasUpdate) {
          //监听小程序有版本更新事件
          updateManager.onUpdateReady(function () {
            //新的版本已经下载好，调用 applyUpdate 应用新版本并重启 （ 此处进行了自动更新操作）
            updateManager.applyUpdate();
          })
          updateManager.onUpdateFailed(function () {
            // 新版本下载失败
            wx.showModal({
              title: '更新提示',
              content: '请您删除当前小程序，到微信 “发现-小程序” 页，重新搜索打开哦~',
            })
          })
        }
      })
    } else {
      //此时微信版本太低（一般而言版本都是支持的）
      wx.showModal({
        title: '溫馨提示',
        content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
      })
    }
  }
}


