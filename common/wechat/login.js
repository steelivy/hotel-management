import store from '@/store';
const install = (Vue, vm) => {
	// 默认高度72
	Vue.prototype.customBar = 72
	return {
		/**
		 * 检查授权登录
		 */
		checkLogin() {
			//链接中是否有授权回调或其他引用传递的openId用户信息
			let openid = this.queryParam('openid');
			let errorcode = this.queryParam('errorcode');
			if (vm.$u.common.isWX() && openid) {
				vm.$u.common.wxUserInfo({ type: 'wechat', errorcode, openId: openid });
			}

			// #ifdef H5
			//授权信息时效性
			let token = store.state.vuex_token;
			let expires_time = store.state.vuex_expires_time;
			var now = (new Date()).getTime();
			let isExpires = ((now - expires_time) > 60000 * 60 * 24 * 7); // 7天
			if (isExpires && !openid) {
				console.log('微信授权已过期')
				this.wxInitLogin()
			}
			return isExpires && !openid;
			// #endif

		},
		/**
		 * 微信授权登录
		 * @param {object} success -成功回调
		 * @param {object} fail -失败回调
		 * @param {string} type -授权类型
		 * @param {boolean} getLinkParams -是否获取链接参数
		 * @param {boolean} getLinkParams -是否获取链接参数
		 */
		wxInitLogin(success, fail, type = 'snsapi_base') {
			if (!this.isWX()) {
				fail && fail('h5')
				return
			}
			// //重定向前端地址
			// let url = vm.$u.queryParams({
			// 	actId: store.state.vuex_actId,
			// 	id: store.state.vuex_id,
			// 	source: store.state.vuex_source,
			// 	type: store.state.vuex_type,
			// },true)
			let redirect_uri = encodeURIComponent(`${location.origin}/pages/webView/auth/index${location.search}`);
			let state = encodeURIComponent(
				("" + Math.random()).split(".")[1] + "authorizestate"
			);
			if (process.env.NODE_ENV == 'development') {
				let domainName = type == 'snsapi_userinfo' ? `lxs=onlinepv` : '';
				redirect_uri = encodeURIComponent('https://xcx.meidaju.cn/route/api/wxauth?' + domainName +
					'&redirect=' + redirect_uri);
			} else {
				let domainName = type == 'snsapi_userinfo' ? `lxs=online` : '';
				redirect_uri = encodeURIComponent('https://xcx.meidaju.cn/route/api/wxauth?' + domainName +
					'&redirect=' + redirect_uri);
			};
			if (type == 'snsapi_userinfo' || type == 'snsapi_base') {
				const authurl =
					`https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxd1fd7d036341280d&redirect_uri=${redirect_uri}&response_type=code&scope=${type}&state=${state}#wechat_redirect`;
				location.href = authurl;
			}
		},
		/**
		 * 获取微信用户信息
		 * @param {Object} params 获取微信授权完成回调信息
		 */
		wxUserInfo(params) {
			let {
				type,
				openId,
				errorcode
			} = params;
			let expires_time = Date.now();
			// 1、微信小程序（第三方）嵌入时，微信H5授权用户状态存储用两个字段存储（'vuex_token',''）。原因：
			// 		1)、openId由小程序（第三方）链接参数传入，所有业务逻辑接口的openId使用小程序（第三方）的。
			// 		2)、但是微信还是要授权，用于获取使用微信相关能力（支付、扫码）。
			// 2、单独在微信H5使用时，用户授权状态存储用一个字段存储（'vuex_token'）即可		
			store.commit('$uStore', { name: 'vuex_token_wx', value: openId });
			store.commit('$uStore', { name: 'vuex_token', value: openId });
			store.commit('$uStore', { name: 'vuex_expires_time', value: expires_time });
			// #ifdef H5
			if (type === 'wechat') {
				history.back();//返回时如果原链接参数有openId（小程序下），则会重新加载app.vue中的queryParams方法覆盖微信授权的openid的状态存储
			}
			// #endif
		},

		getSystemInfo() {
			return new Promise((resolve, reject) => {
				uni.getSystemInfo({
					success: function (e) {
						console.log('版本库', e.SDKVersion)
						Vue.prototype.statusBar = e.statusBarHeight
						// #ifndef MP
						if (e.platform == 'android') {
							Vue.prototype.customBar = e.statusBarHeight + 50
						} else {
							Vue.prototype.customBar = e.statusBarHeight + 45
						}
						// #endif
	
						// #ifdef MP-WEIXIN
						let custom = wx.getMenuButtonBoundingClientRect()
						Vue.prototype.customBar = custom.bottom + custom.top - e.statusBarHeight
						// #endif
	
						// #ifdef MP-ALIPAY
						Vue.prototype.customBar = e.statusBarHeight + e.titleBarHeight
						// #endif
	
						resolve()
					}
				})
			})
		}
	}
}

export default {
	install
}