import store from '@/store';
import device from '@/common/device.js';
import utils from '@/common/utils.js';

const install = (Vue, vm) => {
	return {
		initWechatShare(id, shareLink, type) {
			let scope = this;
			let signLink = utils.filterParam('openid', location.href.split('#')[0]);
			signLink = utils.filterParam('errorcode', signLink);
			let url = encodeURIComponent(signLink)
			const param = {
				id: id,
				shareurl: url,
				sharetype: type,
				openId: store.state.vuex_token_wx,
				contentType: 'form'
			};
			return new Promise((resolve, reject) => {
				if (!device.isWX()) { //不是微信环境
					resolve();
					return
				}
				vm.$u.api.wxShare(param).then(res => {
					shareLink = utils.filterParam('openid', shareLink);
					shareLink = utils.filterParam('errorcode', shareLink);
					let shareConfig = {
						title: res.result.title,
						imgUrl: res.result.logo,
						desc: res.result.description,
						link: shareLink
					}
					//提供分享文案给微信小程序分享
					jWeixin.miniProgram && jWeixin.miniProgram.postMessage({
						data: shareConfig
					})
					scope.wxConfig(res.result).then(() => {
						console.log('wxConfig',res);
						resolve(res);
						// 分享到朋友圈
						jWeixin.onMenuShareTimeline({
							title: shareConfig.title, // 分享标题
							link: shareConfig.link, // 分享链接
							imgUrl: shareConfig.imgUrl, // 分享图标
							success: function() {
								// 用户确认分享后执行的回调函数
								// successShare();
								// mta_handle.MtaShare('wechat_moments');
							},
							cancel: function() {
								// 用户取消分享后执行的回调函数
							}
						});
						// 分享给朋友
						jWeixin.onMenuShareAppMessage({
							title: shareConfig.title, // 分享标题
							desc: shareConfig.desc, // 分享描述
							link: shareConfig.link, // 分享链接
							imgUrl: shareConfig.imgUrl, // 分享图标
							type: "", // 分享类型,music、video或link，不填默认为link
							dataUrl: "", // 如果type是music或video，则要提供数据链接，默认为空
							success: function() {
								// 用户确认分享后执行的回调函数
								// successShare();
								// mta_handle.MtaShare('wechat_friend');
							},
							cancel: function() {
								// 用户取消分享后执行的回调函数
							}
						});
						// 分享到 QQ
						jWeixin.onMenuShareQQ({
							title: shareConfig.title, // 分享标题
							desc: shareConfig.desc,
							link: shareConfig.link, // 分享链接
							imgUrl: shareConfig.imgUrl, // 分享图标
							success: function() {
								// 用户确认分享后执行的回调函数
								// successShare();
							},
							cancel: function() {
								// 用户取消分享后执行的回调函数
							}
						});
						// 分享到 QQ 空间
						jWeixin.onMenuShareQZone({
							title: shareConfig.title, // 分享标题
							desc: shareConfig.desc,
							link: shareConfig.link, // 分享链接
							imgUrl: shareConfig.imgUrl, // 分享图标
							success: function() {
								// 用户确认分享后执行的回调函数
								// successShare();
							},
							cancel: function() {
								// 用户取消分享后执行的回调函数
							}
						});
					}).catch(err => {
						reject(err)
					})
				})
			})
		},
		wxConfig(params) { //微信配置
			return new Promise((resolve, reject) => {
				jWeixin.config({
					debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
					appId: params.wxsign.appid, // 必填，公众号的唯一标识
					timestamp: params.wxsign.timestamp, // 必填，生成签名的时间戳
					nonceStr: params.wxsign.noncestr, // 必填，生成签名的随机串
					signature: params.wxsign.signature, // 必填，签名，见附录1
					jsApiList: [
						'hideOptionMenu', //界面操作接口1
						'showOptionMenu', //界面操作接口2
						'hideMenuItems', //界面操作接口4
						'showMenuItems', //界面操作接口5
						"onMenuShareTimeline",
						"onMenuShareAppMessage",
						"onMenuShareQQ",
						"onMenuShareQZone",
						"getLocation",
						"scanQRCode"
					] ,// 必填，需要使用的JS接口列表，所有JS接口列表见附录2
					openTagList: ['wx-open-subscribe']
				});
				jWeixin.ready(function() {
					// config信息验证后会执行ready方法，所有接口调用都必须在config接口获得结果之后，config是一个客户端的异步操作，所以如果需要在页面加载时就调用相关接口，则须把相关接口放在ready函数中调用来确保正确执行。对于用户触发时才调用的接口，则可以直接调用，不需要放在ready函数中。
					resolve();
				})
				jWeixin.error(function(res) {
					// config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
					reject();
				});
			})
		},
		/**
		 * 微信支付
		 * @param {String} wxtimestamp -签名
		 * @param {String} wxnonceStr -签名
		 * @param {String} wxpackage -签名
		 * @param {String} wxpaySign -签名
		 */
		wxPay: function ({wxappId,wxtimestamp, wxnonceStr, wxpackage, wxpaySign}) {
			return new Promise((resolve,reject)=>{
				WeixinJSBridge.invoke('getBrandWCPayRequest', {
					'appId': wxappId,
					'timeStamp': wxtimestamp,
					'nonceStr': wxnonceStr,
					'package': wxpackage,
					'signType': 'MD5',
					'paySign': wxpaySign
				}, function (res) {
					console.log('wxPay',res)
					if(res.err_msg == "get_brand_wcpay_request:ok" ){
						resolve(res)
					}else{
						reject(res)
					}
				})
			})
		},
	}
}

export default {
	install
}
