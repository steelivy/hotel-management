/*
 *读取env环境变量
 */
const fs = require('fs');
const path = require('path');
//env 判断打包环境指定对应的服务器id
const SERVER_ID = process.env.NODE_ENV === 'prod' ? 1 : 0;
function parse(src) {
  // 解析KEY=VAL的文件
  const res = {};
  src.split('\n').forEach(line => {
    // matching "KEY' and 'VAL' in 'KEY=VAL'
    // eslint-disable-next-line no-useless-escape
    const keyValueArr = line.match(/^\s*([\w\.\-]+)\s*=\s*(.*)?\s*$/);
    // matched?
    if (keyValueArr != null) {
      const key = keyValueArr[1];
      let value = keyValueArr[2] || '';

      // expand newlines in quoted values
      const len = value ? value.length : 0;
      if (len > 0 && value.charAt(0) === '"' && value.charAt(len - 1) === '"') {
        value = value.replace(/\\n/gm, '\n');
      }

      // remove any surrounding quotes and extra spaces
      value = value.replace(/(^['"]|['"]$)/g, '').trim();

      res[key] = value;
    }
  });
  return res;
}

/*
 *定义多个服务器账号 及 根据 SERVER_ID 导出当前环境服务器账号
 */
const SERVER_LIST = [
  {
    id: 1,
    name: '开发测试环境',
    domain: 'xcx.meidaju.cn/',// 域名
    host: '175.178.78.82',// ip
    port: 22,// 端口
    username: 'root', // 登录服务器的账号
    password: 'Yd_2022@WI5v2s',// 登录服务器的账号
    path: '../data/ya'// 发布至静态服务器的项目路径
  },
  // {
  //   id: 1,
  //   name: '开发测试环境',
  //   domain: 'xcx.meidaju.cn/',// 域名
  //   host: '175.178.78.82',// ip
  //   port: 22,// 端口
  //   username: 'root', // 登录服务器的账号
  //   password: 'Yd_2022@WI5vs',// 登录服务器的账号
  //   path: '../data/ya'// 发布至静态服务器的项目路径
  // },
  // {
  //   id: 0,
  //   name: '生产环境',
  //   domain: 'online.salesonline.com.cn',// 域名
  //   host: '159.75.235.48',// ip
  //   port: 22,// 端口
  //   username: 'root', // 登录服务器的账号
  //   password: 'Yd_2022@WI5vs',// 登录服务器的账号
  //   path: '../data/ya'// 发布至静态服务器的项目路径
  // },
];

module.exports = SERVER_LIST[SERVER_ID];