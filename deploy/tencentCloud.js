const fs = require('fs');
var path = require('path');
const chalk = require('chalk');

const COS = require('cos-nodejs-sdk-v5');

const SecretId = "AKIDXQ42n3iXtyoxrWJSh9A0bw0piLirpQow";
const SecretKey = "2LO0amSeYKvPRLZgdsya4JG0ez7D7mDQ";

// 刷新
function refresh() {
  const tencentcloud = require("tencentcloud-sdk-nodejs");
  const CdnClient = tencentcloud.cdn.v20180606.Client;
  let bucket = "amtest";
  if (process.env.NODE_ENV === 'prod') bucket = "amtest";  
  // 实例化一个认证对象，入参需要传入腾讯云账户secretId，secretKey,此处还需注意密钥对的保密
  // 密钥可前往https://console.cloud.tencent.com/cam/capi网站进行获取
  const clientConfig = {
    credential: {
      secretId: SecretId,
      secretKey: SecretKey,
    },
    region: "",
    profile: {
      httpProfile: {
        "endpoint": "cdn.tencentcloudapi.com",
      },
    },
  };
  
  // 实例化要请求产品的client对象,clientProfile是可选的
  const client = new CdnClient(clientConfig);
  const params = {
	"FlushType": 'flush',
	"Paths": [
		"https://" + bucket + "-1258066489.file.myqcloud.com/web/ycang/h5/assets/images/"
	],
  };
  client.PurgePathCache(params).then(
    (data) => {
      console.log(chalk.green("cdn刷新目录成功"));
    },
    (err) => {
      console.error("cdn刷新目录失败", err);
    }
  );

}


//查询指定目录中的文件是否存在
function fileExists(mediaFile) {
  return new Promise((resolve, reject) => {
    fs.exists(mediaFile, function (exists) {
      if (exists) { //文件存在
        resolve();
      } else {
        reject();
      }
    })
  })
}
//查询指定目录中的文件
function readFileList(dir, filesList = []) {
  const files = fs.readdirSync(dir);
  files.forEach((item, index) => {
    var fullPath = path.join(dir, item);
    const stat = fs.statSync(fullPath);
    if (stat.isDirectory()) {
      readFileList(path.join(dir, item), filesList); //递归读取文件
    } else {
      filesList.push(fullPath);
    }
  });
  return filesList;
}

//配置默认参数
let serveConfig = null;
let mediaFile = path.join(__dirname, '../unpackage/dist/build/h5/static/images');
if (process.env.NODE_ENV === 'prod') {
  serveConfig = {
    Bucket: 'amtest-1258066489',
    Region: 'ap-guangzhou',
    Key: 'web/ycang/h5/assets/images',
    refresh: refresh,
  }
} else {
  serveConfig = {
    Bucket: 'amtest-1258066489',
    Region: 'ap-guangzhou',
    Key: 'web/ycang/h5/assets/images',
    // refresh: refresh,
  }
}
if (!serveConfig) return;

// 初始化
let cos = new COS({
  SecretId,
  SecretKey
});
// delAllThenUploadFiles();
async function delAllThenUploadFiles() {
  await fileExists(mediaFile).then(() => {
    lookupFileName();
  }).catch(() => {
    console.log(
      chalk.yellow(
        '暂无图片资源上传！！！'
      )
    )
  })
}

//查找文件名
function lookupFileName() {
  cos.getBucket({
    Bucket: serveConfig.Bucket,
    /* 必须 */
    Region: serveConfig.Region,
    /* 必须 */
    Prefix: serveConfig.Key + '/',
    /* 非必须 */
  }, function (err, data) {
    let keys = [];
    data.Contents.map(item => {
      keys.push({
        Key: item.Key
      })
    })
    if (keys.length) deleteMultipleObject({
      bucketList: keys
    })
    else { //没文件
      uploadFiles();
    }
  });
}

//批量删除
function deleteMultipleObject(params) {
  let {
    bucketList
  } = params;
  cos.deleteMultipleObject({
    Bucket: serveConfig.Bucket,
    /* 必须 */
    Region: serveConfig.Region,
    /* 必须 */
    Objects: bucketList
  }, function (err, data) {
    if (!err) uploadFiles(); //删除没有错误，上传新的资源列表
  });
}

//批量上传图片
function uploadFiles() {
  let uploadFilesList = [];
  // readFileList(path.resolve('dist/static/image'),uploadFilesList);//以执行node的路径开始
  readFileList(mediaFile, uploadFilesList); //以文件所在路径开始
  uploadFilesList = uploadFilesList.map(item => {
    return Object.assign({}, serveConfig, {
      Key: serveConfig.Key + '/' + path.basename(item),
      FilePath: item,
    })
  })
  cos.uploadFiles({
    files: uploadFilesList,
    SliceSize: 1024 * 1024,
    onProgress: function (info) {
      var percent = parseInt(info.percent * 10000) / 100;
      var speed = parseInt(info.speed / 1024 / 1024 * 100) / 100;
      console.log(chalk.yellow('进度：' + percent + '%; 速度：' + speed + 'Mb/s;'));
    },
    onFileFinish: function (err, data, options) {
      console.log(err ? chalk.red(options.Key + '上传失败') : chalk.green(options.Key + '上传完成'));
    },
  }, function (err, data) {
    if (serveConfig.refresh && data) serveConfig.refresh(); //上传成功刷新
    let name = process.env.NODE_ENV === 'prod' ? '生产环境' : '测试环境';
    console.log(err ? chalk.red(name + '上传失败') : chalk.green(name + '上传完成'));
  });
}
module.exports = {
  delAllThenUploadFiles,
  refresh
}