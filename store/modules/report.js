// import api from '@/store/$u.mixin.js';
import Vue from 'vue';
const report = {
	async init(params){
		let {type,data} = params;
		switch(type){
			case 'actSignUp':
				await this.actSignUpReportApi(data);
			break;
		}
	},
	/**
	 * 报名活动上报接口
	 */
	actSignUpReportApi(params){
		if(!params.openId) new Promise.reject('openId必传');
		// 获取不到实例this
		// return this.$u.api.actSignUpReport(params)
	}
}
/**
 *  Vuex 支持 state 模块嵌套。详见文档[https://vuex.vuejs.org/zh-cn/modules.html]
 *  这里是一个子状态树的定义，一个页面对应一个子状态树
 */
/**
 *  定义常量 muations 名
 */

//state
const state = {
}

// getters
const getters = {	
}

// actions
const actions = {
	reportAction({commit,dispatch,rootState,rootGetters,getters,state},params){
		return report.init(params);
	}
}

// mutations
const mutations = {
}

export default {
  state,
  getters,
  actions,
  mutations
}