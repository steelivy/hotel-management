/**
 *  Vuex 支持 state 模块嵌套。详见文档[https://vuex.vuejs.org/zh-cn/modules.html]
 *  这里是一个子状态树的定义，一个页面对应一个子状态树
 */
/**
 *  定义常量 muations 名
 */

//state
const state = {
  toastData: '',//提示框数据
}

// getters
const getters = {
  toastData: state => {
    return state.toastData
  }
	
}

// actions
const actions = {

}

// mutations
const mutations = {
}

export default {
  state,
  getters,
  actions,
  mutations
}