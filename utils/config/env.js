var cloudstore = require("./cloudstore.js");
var ftmsCloudstore = require("./ftmsCloudstore.js");
var gzCloudstore = require("./gzCloudstore.js");
var bjCloudstore = require("./bjCloudstore.js");
var dbCloudstore = require("./dbCloudstore.js");
var nbCloudstore = require("./nbCloudstore.js");
var xbCloudstore = require("./xbCloudstore.js");
var bbCloudstore = require("./bbCloudstore.js");
var zbCloudstore = require("./zbCloudstore.js");
var ttce68b8d55154c63401 = require("./ttce68b8d55154c63401.js");
module.exports = {
  isCustomNav: true, // 是否自定义导航栏
  env: cloudstore,//微云店
  // env: ttce68b8d55154c63401,//ttce68b8d55154c63401
  // env: ftmsCloudstore,//酒店管理
  // env: gzCloudstore,//广州微云店
  // env: bjCloudstore,//北京微云店
  // env: dbCloudstore,//东部微云店
  // env: nbCloudstore,//南部微云店
  // env: xbCloudstore,//西部微云店
  // env: bbCloudstore,//北部微云店
  // env: zbCloudstore,//中部微云店
}