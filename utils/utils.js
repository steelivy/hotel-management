// 获取App.js配置

export default function util(page) {
	let root = page;
	Object.assign(root, {
		// 获取本地用户信息通用方法
		getUserInfo() {
			return new Promise((resolve, reject) => {
				let userInfo = root.$u.common.getEnvStorage('user')
				try {
					userInfo = JSON.parse(userInfo)
					resolve(userInfo)
				} catch (err) {
					resolve({})
				}
			})
		},

		// 用户信息授权通用方法
		onGotUserInfo(e) {//微信授权
			return new Promise((resolve, reject) => {
				let app = getApp()
				let canIUseGetUserProfile = app.globalData.canIUseGetUserProfile
				if (canIUseGetUserProfile) {
					uni.getUserProfile({
						desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
						success: (res) => {
							this.getUserInfoHandle(res, e)
						},
						fail: err => {
							this.$u.common.showFailToast('登录失败')
						}
					})
				} else {
					this.getUserInfoHandle(e.detail, e)
				}
			})
		},

		getUserInfoHandle(res, e) {
			let app = getApp()
			const userInfo = res.userInfo
			if (userInfo == null || userInfo == undefined) {
				this.$u.common.showFailToast('登录失败')
				return
			}
			if (root.getUserInfoCallBack) {
				root.getUserInfoCallBack(res, e)
			}
		},

		// 授权手机号通用方法
		getPhoneNumber(e) {//获取授权手机号
			let app = getApp()
			if (e.detail.errMsg == 'getPhoneNumber:fail user deny') {
			} else if (e.detail.iv) {
				let param = {
					iv: e.detail.iv,
					encryptedData: e.detail.encryptedData,
					key: app.globalData.sessionKey
				}
				this.$u.api.addMobile(param).then(res => {
					root.getPhoneCallBack(res)
				}).catch(err => {
					console.log('上传手机号失败')
				})
			} else {
				this.$u.common.showFailToast('授权手机号失败，请重试')
			}
		},

		goBack() {
			this.$u.common.exitPage()
		}
	})
}
