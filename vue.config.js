const path = require('path');
// const CopyPlugin = require('copy-webpack-plugin');
module.exports = {
	chainWebpack: (config) => {
		//修改复制文件路径
		let {patterns,options} = {
			patterns: [{from: path.join(__dirname,'/static/rootFiles'),to:  path.join(__dirname, '/unpackage/dist/build/h5')}],
			options: {}
		}
		config.plugin('CopyPlugin')
		  .use('copy-webpack-plugin', [patterns,options])//结果同直接使用new CopyPlugin(patterns,options)
		
			
		//图片资源指向cdn
		config.module
			.rule("images")
			.test(/\.(png|jpe?g|gif|svg)(\?.*)?$/)
			.use("url-loader")
			.loader("url-loader")
			.options({
				// 使文件大小小于此limit值(单位为byte)的文件转换为base64格式
				// 大于此limit的, 会执行options中的fallback配置项插件, fallback默认值为file-loader, 
				// 而url-loader的options配置项也会被传递给file-loader
				// start H5开发时使用配置
				limit: 1000000,
				// 根据环境使用cdn或相对路径
				// publicPath: process.env.NODE_ENV === 'production' ? 'https://amtest-1258066489.file.myqcloud.com/web/ycang/h5/assets/images' : '/static/images/',
				// end H5开发时使用配置
				// start 小程序开发时使用配置
				// limit: 8192,//8192kb
				publicPath: 'https://amtest-1258066489.file.myqcloud.com/web/ycang/h5/assets/images',
				// end 小程序开发时使用配置
				// publicPath: process.env.VUE_APP_WEB_STATICS_URL+'/assets/image',
				// 将图片打包到dist/img文件夹下, 不配置则打包到dist文件夹下
				outputPath: 'static/images',
				// 配置打包后图片文件名
				name: '[name].[hash:8].[ext]',
			})
			.end();
	},
	// 配置路径别名
	configureWebpack: {
		devServer: {
			// 调试时允许内网穿透，让外网的人访问到本地调试的H5页面
			disableHostCheck: true,
			
			//mainfest.json配置二选一
			// host: '0.0.0.0',
			// port: 8877,
			// https: false,
			// hotOnly: false,
			// "proxy":  {
			//   "/api": {
			// 	"target": "https://redcartest.instore.com.cn/routeonline",
			// 	"changeOrigin": true,
			// 	"ws": true,  
			// 	"pathRewrite": {
			// 	  "^/api": "/api"   //本身的接口地址没有 '/api' 这种通用前缀，所以要rewrite，如果本身有则去掉  
			// 	}
			//   } 
			// }
		}
	},
	transpileDependencies: ['uview-ui']
	//productionSourceMap: false,
}
